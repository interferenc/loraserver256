package rejoin

import (
	"crypto/aes"
	"encoding/hex"
	"fmt"

	keywrap "github.com/NickBall/go-aes-key-wrap"
	"github.com/pkg/errors"

	"gitlab.com/interferenc/loraserver256/internal/config"
	"gitlab.com/interferenc/lorawan256"
	"gitlab.com/interferenc/lorawan256/backend"
)

// unwrapNSKeyEnveope returns the decrypted key from the given KeyEnvelope.
func unwrapNSKeyEnvelope(ke *backend.KeyEnvelope) (lorawan.AES256Key, error) {
	var key lorawan.AES256Key

	if ke.KEKLabel == "" {
		copy(key[:], ke.AESKey[:])
		return key, nil
	}

	for _, k := range config.C.JoinServer.KEK.Set {
		if k.Label == ke.KEKLabel {
			kek, err := hex.DecodeString(k.KEK)
			if err != nil {
				return key, errors.Wrap(err, "decode kek error")
			}

			block, err := aes.NewCipher(kek)
			if err != nil {
				return key, errors.Wrap(err, "new cipher error")
			}

			b, err := keywrap.Unwrap(block, ke.AESKey[:])
			if err != nil {
				return key, errors.Wrap(err, "unwrap key error")
			}

			copy(key[:], b)
			return key, nil
		}
	}

	return key, fmt.Errorf("unknown kek label: %s", ke.KEKLabel)
}
